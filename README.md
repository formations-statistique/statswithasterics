# Introduction à la statistique avec ASTERICS / Introduction to statistics with ASTERICS

Ce dépôt contient le source des supports de cours de la formation <a href="http://www.nathalievialaneix.eu/teaching/biostat_with_asterics.html" target="_blank">Introduction à la statistique avec ASTERICS</a>.

This repository contains source code for accompanying material of the training session <a href="http://www.nathalievialaneix.eu/teaching/biostat_with_asterics.html" target="_blank">Introduction à la statistique avec ASTERICS</a>. All material is in English.


<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
